use trust_dns_resolver::config::*;
use trust_dns_resolver::TokioAsyncResolver;

use trust_dns_client::rr::{RData, RecordType};

use sha2::{Digest, Sha256};

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let mut opts = ResolverOpts::default();
    opts.validate = true;

    let config = ResolverConfig::quad9();

    let resolver = TokioAsyncResolver::tokio(config, opts)?;

    let mut hasher = Sha256::new();

    hasher.update(b"dkg");

    let result = hex::encode(&hasher.finalize()[0..28]);

    let fqdn = format!("{}._openpgpkey.debian.org", result);
    eprintln!("Looking up {} ...", fqdn);

    let answers = resolver
        .lookup(fqdn, RecordType::OPENPGPKEY, Default::default())
        .await?;

    for record in answers.iter() {
        if let RData::OPENPGPKEY(key) = record {
            use sequoia_openpgp::parse::Parse;
            let cert: Vec<_> =
                sequoia_openpgp::cert::CertParser::from_bytes(key.public_key())?.collect();

            println!("{:?}", cert);
        }
    }

    Ok(())
}
